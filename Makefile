.PHONY: test fmt vet

PROJECT_NAME := translations
FOLDER_BIN = $(CURDIR)/bin
FOLDER_CMD = $(CURDIR)/cmd
GO_TEST = go test ./...
GO_FILES = $(shell go list ./... | grep -v /vendor/)
GO_ENVVARS = DB_HOST=localhost DB_PORT=5432 DB_USER=cableuser DB_PASS=cablepass DB_NAME=cable

air:
	air

start:
	$(GO_ENVVARS) go run $(FOLDER_CMD)/$(PROJECT_NAME)/main.go

build:
	go build -o $(FOLDER_BIN)/$(PROJECT_NAME) $(FOLDER_CMD)/$(PROJECT_NAME)/main.go

lint:
	golangci-lint run ./...

clean-test:
	go clean -testcache

test: clean-test
	$(GO_TEST) -coverprofile cover.out

test-race: clean-test
	$(GO_TEST) -race

test-integration: clean-test
	$(GO_TEST) --tags=integration

fmt:
	go fmt $(GO_FILES)

vet:
	go vet $(GO_FILES)

test-all: fmt vet test-race

mod:
	go mod tidy

sql-migration:
	psql -h localhost -p 5432 -U cableuser -d cable -f ./scripts/sql/schema.sql