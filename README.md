# Cable Translations
This project is a simple exercise of back-end API for managing a word dictionary with translation capabilities. 
The API allows users to add new word translations from one language to another and request translations of words from one
language to another.

## API
The API contains two endpoints:
* `POST /v1/translations` - Creates a new translation for a word.
  * Request body:
    ```json
    {
      "source_word": {
        "value": "string",
        "language": "string"
      },
      "target_word": {
        "value": "string",
        "language": "string"
      }
    }
    ```
    * Response body:
    ```json
    {
    	"status": "string",
    	"data": {
    		"source_word": {
    			"value": "string",
    			"language": "string"
    		},
    		"target_word": {
    			"value": "string",
    			"language": "string"
    		}
    	}
    }
    ```
  * Example:
    ```shell
    curl -d '{"source_word": {"value": "hello","language": "English"},"target_word": {"value": "yarrrr","language": "pirate"}}' -X POST http://127.0.0.1:5001/v1/translation
    
    {"status":"success","data":{"source_word":{"value":"hello","language":"English"},"target_word":{"value":"yarrrr","language":"pirate"}}}
    ```
* `GET /v1/translation?source_word=string&source_language=string&target_language=string` - Gets the translation of a word.
  * Request params:
    * `source_word` - The word to be translated.
    * `source_language` - The language of the word to be translated.
    * `target_language` - The language to translate the word to.
  * Response body:
    ```json
    {
      "status": "string",
      "data": "string"
    }
    ```
  * Example:
    ```shell
    curl -X GET http://127.0.0.1:5001/v1/translation\?source_word\=hola\&source_language\=spanish\&target_language\=portuguese
  
    {"status":"success","data":"olá"}
    ```

## How to run it
### Installation Requirements
Install:
* Go 1.21.* (other versions work as well) - `brew install go`
* Make - `brew install make`
* Docker Compose - `brew install docker-compose`
* psql (optional - for development only) - `brew install postgresql`
* Golangci-lint (optional - for development only) - `brew install golangci-lint`

(Note that this is for MacOS. Do the respective on Linux or Windows)

### Start up DB
```
# Start up the database
docker-compose up -d

# Run the migrations 
make sql-migration
```

(The migrations might ask you for the password. For the postgres image, it's `cablepass`)

### Run the API
```
# start up the API. It will start on port 5001. 
make start
```

You can also build the binary with `make build` and run it with `./bin/translations` but you will need
to set up some env vars for the database. You can check them at the `docker-compose.yaml` file.

### Run tests & linter
```
# Run unit tests
make test

# Run integration tests
make test-integration

# Run the linter
make lint
```

## Future work
As a future work, there's still the chance to implement a few improvements.
For now it didn't make sense to implement it since this is supposed to be a simple exercise.

A few possible features to be added in the future are:
- [ ] Homonym words are not handled on this exercise.
- [ ] Add integration tests on gitlab ci.
- [ ] Add auto generated API schema documentation (swagger).