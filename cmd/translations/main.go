package main

import (
	"context"
	"database/sql"
	"fmt"
	"lab/cable-translations/internal/api"
	"lab/cable-translations/internal/translation"
	"lab/cable-translations/internal/word"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/lib/pq"

	"github.com/go-chi/httplog/v2"
	"github.com/juju/errors"
)

func main() {
	fmt.Println("Running translations...")

	serverAddress := ":5001"

	db, err := initDB()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	logger := initLogger()

	wordRepository := word.NewPGRepository(db)
	dictionary := word.NewDictionary(wordRepository)
	translationHandler := translation.NewHandler(dictionary)

	httpServer := api.NewHTTPServer(logger, serverAddress, translationHandler)

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		fmt.Println("Listening on", serverAddress)
		if err := httpServer.ListenAndServer(); err != nil {
			log.Fatalf("Something went wrong with the server: %s\n", err)
		}
	}()

	<-done

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	if err := httpServer.Shutdown(ctx); err != nil {
		log.Printf("Something went wrong while server was shutting down:%+v\n", err)
		return
	}

	log.Print("HTTPServer stopped properly")
}

func initDB() (*sql.DB, error) {
	dbAddress := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_NAME"),
	)

	fmt.Printf("Trying to connect to db through %s:%s.\n", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"))

	db, err := sql.Open("postgres", dbAddress)
	if err != nil {
		return nil, errors.Trace(err)
	}

	err = db.Ping()
	if err != nil {
		return nil, errors.Trace(err)
	}

	fmt.Println("Connected to PostgreSQL.")

	return db, nil
}

func initLogger() *httplog.Logger {
	env := os.Getenv("API_ENV")
	if env == "" {
		env = "dev"
	}

	logLevel := slog.LevelInfo
	if env == "dev" {
		logLevel = slog.LevelDebug
	}

	logger := httplog.NewLogger("cable-translations", httplog.Options{
		LogLevel:         logLevel,
		Concise:          true,
		RequestHeaders:   true,
		MessageFieldName: "message",
		TimeFieldFormat:  time.DateTime,
		Tags: map[string]string{
			"version": "v1.0.0",
			"env":     env,
		},
	})

	return logger
}
