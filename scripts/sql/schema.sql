CREATE TABLE languages (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) UNIQUE NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE words (
    id SERIAL PRIMARY KEY,
    word VARCHAR(255) NOT NULL,
    language_id INT NOT NULL REFERENCES languages (id),
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    UNIQUE (word, language_id)
);

CREATE INDEX idx_words_language_id_word ON words (word, language_id);

CREATE TABLE translations (
    id SERIAL PRIMARY KEY,
    word_ids INT ARRAY
);

CREATE INDEX idx_translations_word_ids ON translations USING GIN (word_ids);

INSERT INTO languages (name) VALUES
    ('english'),
    ('portuguese'),
    ('spanish'),
    ('french'),
    ('german');

INSERT INTO words (word, language_id) VALUES
    ('hello', 1),
    ('olá', 2),
    ('hola', 3),
    ('bonjour', 4),
    ('guten tag', 5);

INSERT INTO translations (word_ids) VALUES ('{1,2,3,4,5}');
