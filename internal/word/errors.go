package word

import (
	"github.com/juju/errors"
)

// ErrTranslationNotFound is returned when a word translation is not found in the dictionary.
var ErrTranslationNotFound = errors.New("word: translation not found")

// ErrMultipleTranslationsFound is returned when a word has multiple translations in the dictionary.
var ErrMultipleTranslationsFound = errors.New("word: multiple translations found")
