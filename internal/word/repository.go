package word

import (
	"context"
)

// Repository represents a repository for words and its translations.
type Repository interface {
	// AddWord adds a new word.
	AddWord(ctx context.Context, word string, language string) (int, error)
	// AddTranslation adds a new translation between two words.
	AddTranslation(ctx context.Context, wordIDs ...int) error
	// AddLanguage adds a new language to the dictionary.
	AddLanguage(ctx context.Context, language string) error

	// UpdateTranslation updates the translation of a word.
	UpdateTranslation(ctx context.Context, translationID int, wordIDs ...int) error

	// GetTranslationID returns the translation ID for given word IDs.
	GetTranslationID(ctx context.Context, wordIDs ...int) (int, error)
	// GetTranslation returns the translation of a word in a given language.
	GetTranslation(
		ctx context.Context,
		sourceWord string,
		sourceLanguage string,
		targetLanguage string,
	) (string, error)
}
