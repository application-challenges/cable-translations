package word

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/juju/errors"
	"github.com/lib/pq"
)

// PGRepository is a postgres Repository implementation.
type PGRepository struct {
	db *sql.DB
}

// NewPGRepository instantiates a new PGRepository.
func NewPGRepository(db *sql.DB) *PGRepository {
	return &PGRepository{
		db: db,
	}
}

// AddWord adds a new word.
func (r *PGRepository) AddWord(ctx context.Context, word, language string) (int, error) {
	row := r.db.QueryRowContext(ctx, `
		INSERT INTO words (word, language_id) VALUES (
			$1,
        	(SELECT id FROM languages WHERE name = $2)
        )
		ON CONFLICT (word, language_id) DO NOTHING
		RETURNING id;
	`, word, language)
	if row.Err() != nil {
		return 0, errors.Trace(row.Err())
	}

	var wordID int
	err := row.Scan(&wordID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return 0, errors.Trace(err)
	}

	if err == nil {
		return wordID, nil
	}

	err = r.db.QueryRowContext(
		ctx,
		`SELECT id FROM words WHERE word = $1 AND language_id = (SELECT id FROM languages WHERE name = $2);`,
		word,
		language,
	).Scan(&wordID)
	if err != nil {
		return 0, errors.Trace(err)
	}

	return wordID, nil
}

// AddTranslation adds a new translation.
func (r *PGRepository) AddTranslation(ctx context.Context, wordIDs ...int) error {
	if len(wordIDs) == 0 {
		return nil
	}

	_, err := r.db.ExecContext(
		ctx,
		`INSERT INTO translations (word_ids) VALUES ($1)`,
		pq.Array(wordIDs),
	)

	return errors.Trace(err)
}

// AddLanguage adds a new language.
func (r *PGRepository) AddLanguage(ctx context.Context, language string) error {
	_, err := r.db.ExecContext(
		ctx,
		`INSERT INTO languages (name) VALUES ($1) ON CONFLICT (name) DO NOTHING;`,
		language,
	)
	return errors.Trace(err)
}

func (r *PGRepository) UpdateTranslation(ctx context.Context, translationID int, wordIDs ...int) error {
	if len(wordIDs) == 0 {
		return nil
	}

	_, err := r.db.ExecContext(
		ctx,
		`
		UPDATE
   			translations
		SET word_ids = ARRAY(
            SELECT DISTINCT
                UNNEST(ARRAY_CAT(t.word_ids, $1))
            FROM translations t
            WHERE t.id = $2)
		WHERE id = $2`,
		pq.Array(wordIDs),
		translationID,
	)
	return errors.Trace(err)
}

func (r *PGRepository) GetTranslationID(ctx context.Context, wordIDs ...int) (int, error) {
	if len(wordIDs) == 0 {
		return 0, nil
	}

	arger := &arger{}

	query := `SELECT DISTINCT
    			t.id
			from translations t
    			JOIN words w ON w.id = ANY(t.word_ids)
    			JOIN languages l ON l.id = w.language_id
			WHERE `

	for i := 0; i < len(wordIDs); i++ {
		if i != 0 {
			query += " OR "
		}

		query += fmt.Sprintf("w.id = %s", arger.Add(wordIDs[i]))
	}

	rows, err := r.db.QueryContext(ctx, query, arger.args...)
	if err != nil {
		return 0, errors.Trace(err)
	}

	if rows.Err() != nil {
		return 0, errors.Trace(rows.Err())
	}

	defer rows.Close()

	var translationIDs []int
	for rows.Next() {
		var translationID int
		err := rows.Scan(&translationID)
		if err != nil {
			return 0, errors.Trace(err)
		}

		translationIDs = append(translationIDs, translationID)
	}

	if len(translationIDs) == 0 {
		return 0, nil
	}

	if len(translationIDs) > 1 {
		return 0, ErrMultipleTranslationsFound
	}

	return translationIDs[0], nil
}

// GetTranslation returns the translation of a word in a given language.
func (r *PGRepository) GetTranslation(
	ctx context.Context,
	sourceWord string,
	sourceLanguage string,
	targetLanguage string,
) (string, error) {
	row := r.db.QueryRowContext(ctx, `
		WITH hello as (
    		SELECT
        		t.word_ids
    		FROM translations t
        		INNER JOIN words w ON w.id = ANY(t.word_ids)
        		INNER JOIN languages l ON l.id = w.language_id
    		WHERE l.name = $1 AND w.word = $2
		)
		SELECT
    		w.word
		FROM words w
    		INNER JOIN hello h on w.id = any(h.word_ids)
    		INNER JOIN languages l on l.id = w.language_id
		WHERE l.name = $3;
	`, sourceLanguage, sourceWord, targetLanguage)
	if row.Err() != nil {
		return "", errors.Trace(row.Err())
	}

	var translation string
	err := row.Scan(&translation)
	if errors.Is(err, sql.ErrNoRows) {
		return "", ErrTranslationNotFound
	}

	if err != nil {
		return "", errors.Trace(err)
	}

	return translation, nil
}

type arger struct {
	args []interface{}
}

func (a *arger) Add(arg interface{}) interface{} {
	a.args = append(a.args, arg)

	return fmt.Sprintf("$%d", len(a.args))
}

func (a *arger) Args() []interface{} {
	return a.args
}
