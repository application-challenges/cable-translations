package word

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDictionary_AddWordTranslation(t *testing.T) {
	type expected struct {
		error        error
		languages    []string
		translations []int
	}

	tests := []struct {
		name     string
		repo     *mockRepository
		input    Translation
		expected struct {
			error        error
			languages    []string
			translations []int
		}
	}{
		{
			name: "success_new_translation",
			repo: &mockRepository{
				words: map[testWord]int{
					{
						word:     "olá",
						language: "portuguese",
					}: 1,
					{
						word:     "hello",
						language: "english",
					}: 2,
				},
			},
			input: Translation{
				SourceWord: Word{
					Value:    "Olá",
					Language: "Portuguese",
				},
				TargetWord: Word{
					Value:    "hElLo",
					Language: "eNgLiSh",
				},
			},
			expected: expected{
				languages:    []string{"portuguese", "english"},
				translations: []int{1, 2},
			},
		},
		{
			name: "success_update_translation",
			repo: &mockRepository{
				words: map[testWord]int{
					{
						word:     "olá",
						language: "portuguese",
					}: 3,
					{
						word:     "yarrrr",
						language: "pirate",
					}: 4,
				},
			},
			input: Translation{
				SourceWord: Word{
					Value:    "Olá",
					Language: "Portuguese",
				},
				TargetWord: Word{
					Value:    "yarrrr",
					Language: "pirate",
				},
			},
			expected: expected{
				languages:    []string{"portuguese", "pirate"},
				translations: []int{3, 4},
			},
		},
		{
			name: "error_adding_source_language",
			repo: &mockRepository{},
			input: Translation{
				SourceWord: Word{
					Value:    "Olá",
					Language: "First",
				},
			},
			expected: expected{
				error: assert.AnError,
			},
		},
		{
			name: "error_adding_target_language",
			repo: &mockRepository{},
			input: Translation{
				TargetWord: Word{
					Language: "first",
				},
			},
			expected: expected{
				error: assert.AnError,
			},
		},
		{
			name:  "error_adding_source_word",
			repo:  &mockRepository{},
			input: Translation{},
			expected: expected{
				error: assert.AnError,
			},
		},
		{
			name: "error_adding_target_word",
			repo: &mockRepository{
				words: map[testWord]int{
					{
						word:     "olá",
						language: "portuguese",
					}: 1,
				},
			},
			input: Translation{
				SourceWord: Word{
					Value:    "olá",
					Language: "portuguese",
				},
				TargetWord: Word{
					Value:    "hello",
					Language: "english",
				},
			},
			expected: expected{
				error: assert.AnError,
			},
		},
		{
			name: "error_adding_translation",
			repo: &mockRepository{
				words: map[testWord]int{
					{
						word:     "olá",
						language: "portuguese",
					}: 1,
					{
						word:     "hello",
						language: "english",
					}: 1,
				},
			},
			input: Translation{
				SourceWord: Word{
					Value:    "Olá",
					Language: "Portuguese",
				},
				TargetWord: Word{
					Value:    "hElLo",
					Language: "eNgLiSh",
				},
			},
			expected: expected{
				error: assert.AnError,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			repo := tt.repo
			dictionary := NewDictionary(repo)

			err := dictionary.AddWordTranslation(context.Background(), tt.input)

			if err != nil {
				assert.Error(t, tt.expected.error, err)
				return
			}
			assert.Equal(t, tt.expected.error, err)
			assert.Equal(t, tt.expected.languages, repo.languages)
			assert.Equal(t, tt.expected.translations, repo.translations)
		})
	}
}

func TestDictionary_GetWordTranslation(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		repo := &mockRepository{}
		dictionary := NewDictionary(repo)

		translation, err := dictionary.GetWordTranslation(context.Background(), Word{
			Value:    "Olá",
			Language: "Portuguese",
		}, "spaNish")

		assert.Nil(t, err)
		assert.Equal(t, "hola", translation)
	})

	t.Run("error", func(t *testing.T) {
		repo := &mockRepository{}
		dictionary := NewDictionary(repo)

		_, err := dictionary.GetWordTranslation(context.Background(), Word{
			Value:    "Olá",
			Language: "Portuguese",
		}, "pOrtuguese")

		assert.Error(t, assert.AnError, err)
	})
}

type testWord struct {
	word     string
	language string
}

type mockRepository struct {
	languages    []string
	words        map[testWord]int
	translations []int
}

func (m *mockRepository) AddTranslation(ctx context.Context, wordIDs ...int) error {
	if len(wordIDs) == 2 && wordIDs[0] == wordIDs[1] {
		return assert.AnError
	}

	m.translations = append(m.translations, wordIDs...)
	return nil
}

func (m *mockRepository) UpdateTranslation(ctx context.Context, translationID int, wordIDs ...int) error {
	m.translations = append(m.translations, wordIDs...)
	return nil
}

func (m *mockRepository) GetTranslationID(ctx context.Context, wordIDs ...int) (int, error) {
	if len(wordIDs) == 2 && wordIDs[0] == 3 && wordIDs[1] == 4 {
		return 10, nil
	}

	return 0, nil
}

func (m *mockRepository) AddWord(ctx context.Context, word string, language string) (int, error) {
	w := testWord{
		word:     word,
		language: language,
	}

	id, ok := m.words[w]
	if !ok {
		return 0, assert.AnError
	}

	return id, nil
}

func (m *mockRepository) AddLanguage(ctx context.Context, language string) error {
	if language == "first" {
		return assert.AnError
	}

	m.languages = append(m.languages, language)
	return nil
}

func (m *mockRepository) GetTranslation(ctx context.Context, sourceWord string, sourceLanguage string, targetLanguage string) (string, error) {
	if sourceLanguage == targetLanguage {
		return "", assert.AnError
	}

	return "hola", nil
}
