//go:build integration
// +build integration

package word

import (
	"context"
	"database/sql"
	"fmt"
	"lab/cable-translations/internal/pg"
	"testing"

	_ "github.com/lib/pq"

	"github.com/juju/errors"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"github.com/testcontainers/testcontainers-go"
)

func TestPGRepository_AddWord(t *testing.T) {
	container, err := pg.SetupTestDatabase()
	if err != nil {
		t.Fatal(err)
	}

	db, err := initTestDB(container)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		err := pg.TeardownTestDatabase(container, db)
		if err != nil {
			t.Error(err)
		}
	}()

	repository := NewPGRepository(db)

	t.Run("success", func(t *testing.T) {
		count, err := countWords(t, db)
		if err != nil {
			t.Error(err)
		}

		_, err = getWordID(t, db, "pg")
		assert.EqualError(t, sql.ErrNoRows, err.Error())

		id, err := repository.AddWord(context.Background(), "pg", "english")
		assert.Nil(t, err)
		assert.Equal(t, count+1, id)

		_, err = getWordID(t, db, "pg")
		assert.Nil(t, err)
	})

	t.Run("duplicate", func(t *testing.T) {
		count, err := countWords(t, db)
		if err != nil {
			t.Error(err)
		}

		_, err = getWordID(t, db, "pg")
		assert.Nil(t, err)

		id, err := repository.AddWord(context.Background(), "pg", "english")
		assert.Nil(t, err)
		assert.Equal(t, count, id)
	})

	t.Run("error", func(t *testing.T) {
		_, err := repository.AddWord(context.Background(), "clouds", "australian")
		assert.Error(t, err)
	})
}

func TestPGRepository_AddTranslation(t *testing.T) {
	container, err := pg.SetupTestDatabase()
	if err != nil {
		t.Fatal(err)
	}

	db, err := initTestDB(container)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		err := pg.TeardownTestDatabase(container, db)
		if err != nil {
			t.Error(err)
		}
	}()

	repository := NewPGRepository(db)

	t.Run("success", func(t *testing.T) {
		iceID, err := repository.AddWord(context.Background(), "ice", "english")
		assert.Nil(t, err)
		geloID, err := repository.AddWord(context.Background(), "gelo", "portuguese")
		assert.Nil(t, err)

		err = repository.AddTranslation(context.Background(), iceID, geloID)
		assert.Nil(t, err)
	})
}

func TestPGRepository_AddLanguage(t *testing.T) {
	container, err := pg.SetupTestDatabase()
	if err != nil {
		t.Fatal(err)
	}

	db, err := initTestDB(container)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		err := pg.TeardownTestDatabase(container, db)
		if err != nil {
			t.Error(err)
		}
	}()

	repository := NewPGRepository(db)

	t.Run("success", func(t *testing.T) {
		_, err := getLanguageID(t, db, "italian")
		assert.EqualError(t, sql.ErrNoRows, err.Error())

		err = repository.AddLanguage(context.Background(), "italian")
		assert.Nil(t, err)

		_, err = getLanguageID(t, db, "italian")
		assert.Nil(t, err)
	})

	t.Run("duplicate", func(t *testing.T) {
		_, err = getLanguageID(t, db, "italian")
		assert.Nil(t, err)

		err = repository.AddLanguage(context.Background(), "italian")
		assert.Nil(t, err)
	})
}

func TestPGRepository_UpdateTranslation(t *testing.T) {
	container, err := pg.SetupTestDatabase()
	if err != nil {
		t.Fatal(err)
	}

	db, err := initTestDB(container)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		err := pg.TeardownTestDatabase(container, db)
		if err != nil {
			t.Error(err)
		}
	}()

	repository := NewPGRepository(db)

	var helloID int
	var helloTranslationID int

	err = repository.db.QueryRow("SELECT id FROM words WHERE word = 'hello'").
		Scan(&helloID)
	assert.Nil(t, err)

	err = db.QueryRow("SELECT id FROM translations WHERE word_ids @> $1", pq.Array([]int{helloID})).
		Scan(&helloTranslationID)
	assert.Nil(t, err)

	t.Run("success", func(t *testing.T) {
		err = repository.AddLanguage(context.Background(), "pirate")
		assert.Nil(t, err)

		wordID, err := repository.AddWord(context.Background(), "yarrr", "pirate")
		assert.Nil(t, err)

		err = repository.UpdateTranslation(context.Background(), helloTranslationID, helloID, wordID)
		assert.Nil(t, err)

		var expectedID int
		err = db.QueryRow("SELECT id FROM translations WHERE word_ids @> $1", pq.Array([]int{wordID})).
			Scan(&expectedID)
		assert.Nil(t, err)
		assert.Equal(t, expectedID, helloTranslationID)
	})

	t.Run("empty_words", func(t *testing.T) {
		var ids pq.Int64Array
		err := repository.db.QueryRow("SELECT word_ids FROM translations WHERE id = $1", helloTranslationID).
			Scan(&ids)
		assert.Nil(t, err)

		err = repository.UpdateTranslation(context.Background(), helloTranslationID)
		assert.Nil(t, err)

		var expectedIDs pq.Int64Array
		err = repository.db.QueryRow("SELECT word_ids FROM translations WHERE id = $1", helloTranslationID).
			Scan(&expectedIDs)
		assert.Nil(t, err)
		assert.Equal(t, expectedIDs, ids)
	})
}

func TestPGRepository_GetTranslationID(t *testing.T) {
	container, err := pg.SetupTestDatabase()
	if err != nil {
		t.Fatal(err)
	}

	db, err := initTestDB(container)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		err := pg.TeardownTestDatabase(container, db)
		if err != nil {
			t.Error(err)
		}
	}()

	repository := NewPGRepository(db)

	var helloID int
	var helloTranslationID int

	err = repository.db.QueryRow("SELECT id FROM words WHERE word = 'hello'").
		Scan(&helloID)
	assert.Nil(t, err)

	err = db.QueryRow("SELECT id FROM translations WHERE word_ids @> $1", pq.Array([]int{helloID})).
		Scan(&helloTranslationID)
	assert.Nil(t, err)

	t.Run("empty_words", func(t *testing.T) {
		id, err := repository.GetTranslationID(context.Background())
		assert.Nil(t, err)
		assert.Equal(t, 0, id)
	})

	t.Run("success", func(t *testing.T) {
		translationID, err := repository.GetTranslationID(context.Background(), helloID)
		assert.Nil(t, err)
		assert.Equal(t, helloTranslationID, translationID)
	})

	t.Run("success_multiple_words", func(t *testing.T) {
		err := repository.UpdateTranslation(context.Background(), 999, helloID)
		assert.Nil(t, err)

		translationID, err := repository.GetTranslationID(context.Background(), 999, helloID)
		assert.Nil(t, err)
		assert.Equal(t, helloTranslationID, translationID)
	})

	t.Run("error_multiple_translation", func(t *testing.T) {
		err := repository.AddTranslation(context.Background(), 999, helloID)
		assert.Nil(t, err)

		_, err = repository.GetTranslationID(context.Background(), helloID)
		assert.ErrorIs(t, err, ErrMultipleTranslationsFound)
	})
}

func TestPGRepository_GetTranslation(t *testing.T) {
	container, err := pg.SetupTestDatabase()
	if err != nil {
		t.Fatal(err)
	}

	db, err := initTestDB(container)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		err := pg.TeardownTestDatabase(container, db)
		if err != nil {
			t.Error(err)
		}
	}()

	repository := NewPGRepository(db)

	enID, err := repository.AddWord(context.Background(), "en", "english")
	assert.Nil(t, err)
	ptID, err := repository.AddWord(context.Background(), "pt", "portuguese")
	assert.Nil(t, err)
	esID, err := repository.AddWord(context.Background(), "es", "spanish")
	assert.Nil(t, err)

	_, err = repository.AddWord(context.Background(), "adios", "spanish")
	assert.Nil(t, err)

	err = repository.AddTranslation(context.Background(), enID, ptID, esID)
	assert.Nil(t, err)

	t.Run("success", func(t *testing.T) {
		translation, err := repository.GetTranslation(context.Background(), "en", "english", "portuguese")
		assert.Nil(t, err)
		assert.Equal(t, "pt", translation)

		translation, err = repository.GetTranslation(context.Background(), "pt", "portuguese", "spanish")
		assert.Nil(t, err)
		assert.Equal(t, "es", translation)

		translation, err = repository.GetTranslation(context.Background(), "es", "spanish", "english")
		assert.Nil(t, err)
		assert.Equal(t, "en", translation)
	})

	t.Run("error", func(t *testing.T) {
		_, err := repository.GetTranslation(context.Background(), "adios", "spanish", "portuguese")
		assert.Error(t, err)
	})
}

func countWords(t *testing.T, db *sql.DB) (int, error) {
	t.Helper()

	var count int
	err := db.QueryRow("SELECT COUNT(*) FROM words").Scan(&count)
	if err != nil {
		return 0, errors.Trace(err)
	}

	return count, nil
}

func getWordID(t *testing.T, db *sql.DB, word string) (int, error) {
	t.Helper()

	var id int
	err := db.QueryRow("SELECT id FROM words WHERE word = $1", word).Scan(&id)
	if err != nil {
		return 0, errors.Trace(err)
	}

	return id, nil
}

func getLanguageID(t *testing.T, db *sql.DB, language string) (int, error) {
	t.Helper()

	var id int
	err := db.QueryRow("SELECT id FROM languages WHERE name = $1", language).Scan(&id)
	if err != nil {
		return 0, errors.Trace(err)
	}

	return id, nil
}

func initTestDB(dbContainer testcontainers.Container) (*sql.DB, error) {
	host, err := dbContainer.Host(context.Background())
	if err != nil {
		return nil, errors.Trace(err)
	}

	port, err := dbContainer.MappedPort(context.Background(), "5432")
	if err != nil {
		return nil, errors.Trace(err)
	}

	dbURI := fmt.Sprintf("postgres://postgres:postgres@%v:%v/testdb?sslmode=disable", host, port.Port())

	db, err := sql.Open("postgres", dbURI)
	if err != nil {
		return nil, errors.Trace(err)
	}

	err = db.Ping()
	if err != nil {
		return nil, errors.Trace(err)
	}

	err = pg.MigrateDB(db)
	if err != nil {
		_ = pg.TeardownTestDatabase(dbContainer, db)
		return nil, errors.Trace(err)
	}

	return db, nil
}
