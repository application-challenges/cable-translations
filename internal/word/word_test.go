package word

import (
	"testing"
)

func TestTranslation_Valid(t *testing.T) {
	tests := []struct {
		name        string
		translation Translation
		want        bool
	}{
		{
			name: "success",
			translation: Translation{
				SourceWord: Word{
					Value:    "hi",
					Language: "english",
				},
				TargetWord: Word{
					Value:    "sup",
					Language: "Klingon",
				},
			},
			want: true,
		},
		{
			name: "source_word_empty",
			translation: Translation{
				SourceWord: Word{
					Language: "english",
				},
				TargetWord: Word{
					Value:    "sup",
					Language: "Klingon",
				},
			},
			want: false,
		},
		{
			name: "source_language_empty",
			translation: Translation{
				SourceWord: Word{
					Value: "hi",
				},
				TargetWord: Word{
					Value:    "sup",
					Language: "Klingon",
				},
			},
			want: false,
		},
		{
			name: "target_word_empty",
			translation: Translation{
				SourceWord: Word{
					Value:    "hi",
					Language: "english",
				},
				TargetWord: Word{
					Language: "Klingon",
				},
			},
			want: false,
		},
		{
			name: "target_language_empty",
			translation: Translation{
				SourceWord: Word{
					Value:    "hi",
					Language: "english",
				},
				TargetWord: Word{
					Value: "sup",
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t1 *testing.T) {
			if got := tt.translation.Valid(); got != tt.want {
				t1.Errorf("Valid() = %v, want %v", got, tt.want)
			}
		})
	}
}
