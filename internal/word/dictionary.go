package word

import (
	"context"
	"strings"

	"github.com/juju/errors"
)

// Dictionary is a struct that holds language translations between words.
type Dictionary struct {
	repository Repository
}

// NewDictionary instantiates a new Dictionary.
func NewDictionary(repository Repository) *Dictionary {
	return &Dictionary{
		repository: repository,
	}
}

// AddWordTranslation adds a new translation to the dictionary.
func (d *Dictionary) AddWordTranslation(ctx context.Context, translation Translation) error {
	if err := d.addLanguage(ctx, strings.ToLower(translation.SourceWord.Language)); err != nil {
		return errors.Trace(err)
	}

	if err := d.addLanguage(ctx, strings.ToLower(translation.TargetWord.Language)); err != nil {
		return errors.Trace(err)
	}

	sourceWordID, err := d.repository.AddWord(
		ctx,
		strings.ToLower(translation.SourceWord.Value),
		strings.ToLower(translation.SourceWord.Language),
	)
	if err != nil {
		return errors.Trace(err)
	}

	targetWordID, err := d.repository.AddWord(
		ctx, strings.ToLower(translation.TargetWord.Value),
		strings.ToLower(translation.TargetWord.Language),
	)
	if err != nil {
		return errors.Trace(err)
	}

	if err := d.addTranslation(ctx, sourceWordID, targetWordID); err != nil {
		return errors.Trace(err)
	}

	return nil
}

// GetWordTranslation returns the translation of a word in a given language.
func (d *Dictionary) GetWordTranslation(ctx context.Context, word Word, language string) (string, error) {
	translation, err := d.repository.GetTranslation(
		ctx,
		strings.ToLower(word.Value),
		strings.ToLower(word.Language),
		strings.ToLower(language),
	)
	if err != nil {
		return "", errors.Trace(err)
	}

	return translation, nil
}

func (d *Dictionary) addLanguage(ctx context.Context, language string) error {
	err := d.repository.AddLanguage(ctx, language)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

func (d *Dictionary) addTranslation(ctx context.Context, sourceWordID, targetWordID int) error {
	translationID, err := d.repository.GetTranslationID(ctx, sourceWordID, targetWordID)
	if err != nil {
		return errors.Trace(err)
	}

	if translationID != 0 {
		err := d.repository.UpdateTranslation(ctx, translationID, sourceWordID, targetWordID)
		return errors.Trace(err)
	}

	err = d.repository.AddTranslation(ctx, sourceWordID, targetWordID)

	return errors.Trace(err)
}
