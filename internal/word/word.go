package word

import (
	"regexp"
)

// Word represents a word in a specific language.
type Word struct {
	Value    string `json:"value"`
	Language string `json:"language"`
}

// New creates a new Word.
func New(word, language string) Word {
	return Word{
		Value:    word,
		Language: language,
	}
}

// Translation represents a translation of a word.
type Translation struct {
	SourceWord Word `json:"source_word"`
	TargetWord Word `json:"target_word"`
}

// Valid checks if the translation is valid.
func (t Translation) Valid() bool {
	return t.SourceWord.Value != "" &&
		t.TargetWord.Value != "" &&
		t.SourceWord.Language != "" &&
		t.TargetWord.Language != "" &&
		onlyLetters.MatchString(t.SourceWord.Value) &&
		onlyLetters.MatchString(t.TargetWord.Value) &&
		onlyLetters.MatchString(t.SourceWord.Language) &&
		onlyLetters.MatchString(t.TargetWord.Language)
}

var onlyLetters = regexp.MustCompile("^[a-zA-Z]+$")
