package api

import (
	"encoding/json"
	"log/slog"
	"net/http"

	"github.com/juju/errors"
)

type response struct {
	Status string      `json:"status"`
	Data   interface{} `json:"data,omitempty"`
	Error  *errorData  `json:"error,omitempty"`
}

type errorData struct {
	ErrorMessage string `json:"error_message,omitempty"`
	ErrorCode    int    `json:"error_code,omitempty"`
}

func newErrorData(message string, code int) errorData {
	return errorData{
		ErrorMessage: message,
		ErrorCode:    code,
	}
}

// Ok builds a success api response with the given data and writes it to the response writer.
func Ok(w http.ResponseWriter, data interface{}) {
	w.WriteHeader(http.StatusOK)

	err := json.NewEncoder(w).Encode(response{
		Status: "success",
		Data:   data,
	})
	if err != nil {
		slog.Error(err.Error())
	}
}

// Error builds an error api response with the given message and code and writes it to
// the response writer.
func Error(w http.ResponseWriter, message string, code int) {
	apiErr := newErrorData(message, code)

	w.WriteHeader(code)

	err := json.NewEncoder(w).Encode(response{
		Status: "error",
		Error:  &apiErr,
	})
	if err != nil {
		slog.Error(err.Error())
	}
}

// InternalServerError builds an internal server error api response with the static message
// and writes it to the response writer.
// The response message doesn't correspond to the actual error message so that we don't
// expose any internal sensitive information from the error.
func InternalServerError(w http.ResponseWriter, err error) {
	slog.Error(errors.ErrorStack(err))
	Error(w, "something went very wrong...", http.StatusInternalServerError)
}
