package middleware

import (
	"context"
	"net/http"
	"time"
)

func Context(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		deadline := time.Now().Add(60 * time.Second)

		ctx, cancel := context.WithDeadline(r.Context(), deadline)
		defer cancel()

		r = r.WithContext(ctx)

		h.ServeHTTP(w, r)
	})
}
