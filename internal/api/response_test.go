package api

import (
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestResponse_Ok(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		responseWriter := mockResponseWriter{
			header: new(int),
			data:   make([]byte, len(`{"status":"success","data":"pg"}`)+1),
		}

		Ok(responseWriter, "pg")

		assert.Equal(t, http.StatusOK, *responseWriter.header)
		assert.Equal(t, `{"status":"success","data":"pg"}`, strings.Trim(string(responseWriter.data), "\n"))
	})
}

func TestResponse_Error(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		responseWriter := mockResponseWriter{
			header: new(int),
			data:   make([]byte, len(`{"status":"error","error":{"error_message":"pg","error_code":400}}`)+1),
		}

		Error(responseWriter, "pg", http.StatusBadRequest)

		assert.Equal(t, http.StatusBadRequest, *responseWriter.header)
		assert.Equal(t, `{"status":"error","error":{"error_message":"pg","error_code":400}}`, strings.Trim(string(responseWriter.data), "\n"))
	})
}

func TestResponse_InternalServerError(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		responseWriter := mockResponseWriter{
			header: new(int),
			data:   make([]byte, len(`{"status":"error","error":{"error_message":"something went very wrong...","error_code":500}}`)+1),
		}

		InternalServerError(responseWriter, assert.AnError)

		assert.Equal(t, http.StatusInternalServerError, *responseWriter.header)
		assert.Equal(
			t,
			`{"status":"error","error":{"error_message":"something went very wrong...","error_code":500}}`,
			strings.Trim(string(responseWriter.data), "\n"),
		)
	})
}

type mockResponseWriter struct {
	header *int
	data   []byte
}

func (m mockResponseWriter) Header() http.Header {
	return http.Header{}
}

func (m mockResponseWriter) Write(bytes []byte) (int, error) {
	for i := 0; i < len(bytes); i++ {
		m.data[i] = bytes[i]
	}
	return len(bytes), nil
}

func (m mockResponseWriter) WriteHeader(statusCode int) {
	*m.header = statusCode
}
