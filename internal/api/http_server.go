package api

import (
	"context"
	"lab/cable-translations/internal/api/middleware"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/httplog/v2"
)

type TranslationHandler interface {
	AddRoutes(r chi.Router) chi.Router
}

// HTTPServer represents an HTTP server for the API.
type HTTPServer struct {
	srv *http.Server

	translationHandler TranslationHandler
}

// NewHTTPServer creates a new HTTP server with the given address.
func NewHTTPServer(logger *httplog.Logger, address string, translationHandler TranslationHandler) *HTTPServer {
	r := chi.NewRouter()

	r.Use(middleware.Context, httplog.RequestLogger(logger))
	r.Route("/v1", func(r chi.Router) {
		translationHandler.AddRoutes(r)
	})

	translationHandler.AddRoutes(r)

	return &HTTPServer{
		srv: &http.Server{
			Addr:         address,
			WriteTimeout: time.Second * 60,
			ReadTimeout:  time.Second * 60,
			IdleTimeout:  time.Second * 60,
			Handler:      r,
		},

		translationHandler: translationHandler,
	}
}

// ListenAndServer starts the server.
func (s *HTTPServer) ListenAndServer() error {
	return s.srv.ListenAndServe()
}

// Shutdown stops the server.
func (s *HTTPServer) Shutdown(ctx context.Context) error {
	return s.srv.Shutdown(ctx)
}
