package translation

import (
	"context"
	"lab/cable-translations/internal/word"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandler_handleTranslation(t *testing.T) {
	tests := []struct {
		name     string
		url      string
		code     int
		response string
	}{
		{
			name:     "success",
			url:      "/v1/translation?source_word=hello&source_language=en&target_language=es",
			code:     http.StatusOK,
			response: `{"status":"success","data":"hola"}`,
		},
		{
			name:     "source_word_required",
			url:      "/v1/translation",
			code:     http.StatusBadRequest,
			response: `{"status":"error","error":{"error_message":"bad request: source_word is required as a query param","error_code":400}}`,
		},
		{
			name:     "source_language_required",
			url:      "/v1/translation?source_word=hello",
			code:     http.StatusBadRequest,
			response: `{"status":"error","error":{"error_message":"bad request: source_language is required as a query param","error_code":400}}`,
		},
		{
			name:     "target_word_required",
			url:      "/v1/translation?source_word=hello&source_language=en",
			code:     http.StatusBadRequest,
			response: `{"status":"error","error":{"error_message":"bad request: target_language is required as a query param","error_code":400}}`,
		},
		{
			name:     "internal_server_error",
			url:      "/v1/translation?source_word=error&source_language=error&target_language=error",
			code:     http.StatusInternalServerError,
			response: `{"status":"error","error":{"error_message":"something went very wrong...","error_code":500}}`,
		},
		{
			name:     "invalid_source_word",
			url:      "/v1/translation?source_word=123&source_language=en&target_language=es",
			code:     http.StatusBadRequest,
			response: `{"status":"error","error":{"error_message":"bad request: source_word must contain only letters","error_code":400}}`,
		},
		{
			name:     "invalid_source_language",
			url:      "/v1/translation?source_word=hi&source_language=11&target_language=es",
			code:     http.StatusBadRequest,
			response: `{"status":"error","error":{"error_message":"bad request: source_language must contain only letters","error_code":400}}`,
		},
		{
			name:     "invalid_target_word",
			url:      "/v1/translation?source_word=hi&source_language=pt&target_language=e1",
			code:     http.StatusBadRequest,
			response: `{"status":"error","error":{"error_message":"bad request: target_language must contain only letters","error_code":400}}`,
		},
	}

	handler := NewHandler(&mockWordDictionary{})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", tt.url, nil)
			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()
			httpHandler := http.HandlerFunc(handler.handleTranslation)
			httpHandler.ServeHTTP(rr, req)

			assert.Equal(t, tt.code, rr.Code)
			assert.Equal(t, tt.response, strings.Trim(rr.Body.String(), "\n"))
		})
	}
}

func TestHandler_handleAddTranslation(t *testing.T) {
	tests := []struct {
		name        string
		translation string
		code        int
		response    string
	}{
		{
			name:        "success",
			translation: `{"source_word": {"value": "sky","language": "English"},"target_word": {"value":"ceu","language": "portuguese"}}`,
			code:        http.StatusOK,
			response:    `{"status":"success","data":{"source_word":{"value":"sky","language":"English"},"target_word":{"value":"ceu","language":"portuguese"}}}`,
		},
		{
			name:        "error_parsing_request",
			translation: `{abc}`,
			code:        http.StatusBadRequest,
			response:    `{"status":"error","error":{"error_message":"bad request: json body is invalid","error_code":400}}`,
		},
		{
			name:        "invalid_request",
			translation: `{"source_word": {"value": "","language": ""},"target_word": {"value":"","language": ""}}`,
			code:        http.StatusBadRequest,
			response:    `{"status":"error","error":{"error_message":"bad request: translation is invalid","error_code":400}}`,
		},
		{
			name:        "internal_server_error",
			translation: `{"source_word": {"value": "error","language": "error"},"target_word": {"value":"error","language": "error"}}`,
			code:        http.StatusInternalServerError,
			response:    `{"status":"error","error":{"error_message":"something went very wrong...","error_code":500}}`,
		},
		{
			name:        "invalid_source_word",
			translation: `{"source_word": {"value": "123","language": "English"},"target_word": {"value":"ceu","language": "portuguese"}}`,
			code:        http.StatusBadRequest,
			response:    `{"status":"error","error":{"error_message":"bad request: translation is invalid","error_code":400}}`,
		},
		{
			name:        "invalid_source_language",
			translation: `{"source_word": {"value": "sky","language": "11"},"target_word": {"value":"ceu","language": "portuguese"}}`,
			code:        http.StatusBadRequest,
			response:    `{"status":"error","error":{"error_message":"bad request: translation is invalid","error_code":400}}`,
		},
		{
			name:        "invalid_target_word",
			translation: `{"source_word": {"value": "sky","language": "English"},"target_word": {"value":"123","language": "portuguese"}}`,
			code:        http.StatusBadRequest,
			response:    `{"status":"error","error":{"error_message":"bad request: translation is invalid","error_code":400}}`,
		},
		{
			name:        "invalid_target_language",
			translation: `{"source_word": {"value": "sky","language": "English"},"target_word": {"value":"ceu","language": "11"}}`,
			code:        http.StatusBadRequest,
			response:    `{"status":"error","error":{"error_message":"bad request: translation is invalid","error_code":400}}`,
		},
	}

	handler := NewHandler(&mockWordDictionary{})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("POST", "/v1/translation", strings.NewReader(tt.translation))
			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()
			httpHandler := http.HandlerFunc(handler.handleAddTranslation)
			httpHandler.ServeHTTP(rr, req)

			assert.Equal(t, tt.code, rr.Code)
			assert.Equal(t, tt.response, strings.Trim(rr.Body.String(), "\n"))
		})
	}
}

type mockWordDictionary struct{}

func (m mockWordDictionary) AddWordTranslation(ctx context.Context, translation word.Translation) error {
	if translation.SourceWord.Value == "error" {
		return assert.AnError
	}

	return nil
}

func (m mockWordDictionary) GetWordTranslation(ctx context.Context, word word.Word, language string) (string, error) {
	if word.Value == "error" {
		return "", assert.AnError
	}

	return "hola", nil
}
