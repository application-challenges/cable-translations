package translation

import (
	"context"
	"encoding/json"
	"lab/cable-translations/internal/api"
	"lab/cable-translations/internal/word"
	"net/http"
	"net/url"
	"regexp"

	"github.com/go-chi/chi/v5"
	"github.com/juju/errors"
)

// WordDictionary represents a dictionary of words.
type WordDictionary interface {
	AddWordTranslation(ctx context.Context, translation word.Translation) error
	GetWordTranslation(ctx context.Context, word word.Word, language string) (string, error)
}

// Handler represents an HTTP handler for the translation API.
type Handler struct {
	dictionary WordDictionary
}

// NewHandler creates a new handler for the translation API.
func NewHandler(dictionary WordDictionary) *Handler {
	return &Handler{
		dictionary: dictionary,
	}
}

// AddRoutes adds the routes for the translation API.
func (h *Handler) AddRoutes(r chi.Router) chi.Router {
	return r.Route("/translation", func(r chi.Router) {
		r.Get("/", h.handleTranslation)
		r.Post("/", h.handleAddTranslation)
	})
}

func (h *Handler) handleTranslation(writer http.ResponseWriter, request *http.Request) {
	ctx := request.Context()

	var (
		sourceWord     string
		sourceLanguage string
		targetLanguage string
	)

	params := request.URL.Query()

	sourceWord, err := getSourceWord(params)
	if err != nil {
		api.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	sourceLanguage, err = getSourceLanguage(params)
	if err != nil {
		api.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	targetLanguage, err = getTargetLanguage(params)
	if err != nil {
		api.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	w := word.New(sourceWord, sourceLanguage)

	translation, err := h.dictionary.GetWordTranslation(ctx, w, targetLanguage)
	if errors.Is(err, word.ErrTranslationNotFound) {
		api.Error(writer, "translation not found", http.StatusNotFound)
		return
	}

	if err != nil {
		api.InternalServerError(writer, err)
		return
	}

	api.Ok(writer, translation)
}

func (h *Handler) handleAddTranslation(writer http.ResponseWriter, request *http.Request) {
	ctx := request.Context()

	var translation word.Translation

	err := json.NewDecoder(request.Body).Decode(&translation)
	if err != nil {
		api.Error(writer, "bad request: json body is invalid", http.StatusBadRequest)
		return
	}

	if !translation.Valid() {
		api.Error(writer, "bad request: translation is invalid", http.StatusBadRequest)
		return
	}

	err = h.dictionary.AddWordTranslation(ctx, translation)
	if err != nil {
		api.InternalServerError(writer, err)
		return
	}

	api.Ok(writer, translation)
}

func getSourceWord(params url.Values) (string, error) {
	sourceWord := params.Get("source_word")
	if sourceWord == "" {
		return "", errors.New("bad request: source_word is required as a query param")
	}

	if !onlyLetters.MatchString(sourceWord) {
		return "", errors.New("bad request: source_word must contain only letters")
	}

	return sourceWord, nil
}

func getSourceLanguage(params url.Values) (string, error) {
	sourceLanguage := params.Get("source_language")
	if sourceLanguage == "" {
		return "", errors.New("bad request: source_language is required as a query param")
	}

	if !onlyLetters.MatchString(sourceLanguage) {
		return "", errors.New("bad request: source_language must contain only letters")
	}

	return sourceLanguage, nil
}

func getTargetLanguage(params url.Values) (string, error) {
	targetLanguage := params.Get("target_language")
	if targetLanguage == "" {
		return "", errors.New("bad request: target_language is required as a query param")
	}

	if !onlyLetters.MatchString(targetLanguage) {
		return "", errors.New("bad request: target_language must contain only letters")
	}

	return targetLanguage, nil
}

var onlyLetters = regexp.MustCompile("^[a-zA-Z]+$")
