package pg

import (
	"context"
	"database/sql"
	"fmt"
	"os"

	"github.com/juju/errors"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

// SetupTestDatabase creates a new database container and returns a connection to it.
func SetupTestDatabase() (testcontainers.Container, error) {
	containerReq := testcontainers.ContainerRequest{
		Image:        "postgres:15.4-alpine",
		ExposedPorts: []string{"5432/tcp"},
		WaitingFor:   wait.ForListeningPort("5432/tcp"),
		Env: map[string]string{
			"POSTGRES_DB":       "testdb",
			"POSTGRES_USER":     "postgres",
			"POSTGRES_PASSWORD": "postgres",
		},
	}

	return testcontainers.GenericContainer(
		context.Background(),
		testcontainers.GenericContainerRequest{
			ContainerRequest: containerReq,
			Started:          true,
			Logger:           noLogger{},
		})
}

// MigrateDB runs the schema.sql script on the database.
func MigrateDB(db *sql.DB) error {
	fmt.Println(os.Getwd())
	data, err := os.ReadFile("../../scripts/sql/schema.sql")
	if err != nil {
		return errors.Trace(err)
	}

	_, err = db.Exec(string(data))
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

// TeardownTestDatabase drops all tables in the database and terminates the container.
func TeardownTestDatabase(dbContainer testcontainers.Container, db *sql.DB) error {
	_, err := db.Exec(`select 'drop table if exists "' || tablename || '" cascade;' 
				from pg_tables
			where schemaname = 'public'
	`)
	if err != nil {
		return errors.Trace(err)
	}

	err = db.Close()
	if err != nil {
		return errors.Trace(err)
	}

	err = dbContainer.Terminate(context.Background())
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

type noLogger struct{}

func (n noLogger) Printf(_ string, _ ...interface{}) {}
